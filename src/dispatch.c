#include <X11/Xlib.h>
#include <configure.h>
#include <dispatch.h>
#include <stdio.h>

int dispatch(Display *dpy, Window win, GC gc) {
  int NoFillFace = 0;
  XEvent event;
  int end = 0;
  while (!end) {
    XNextEvent(dpy, &event);
    switch (event.type) {
    case Expose:
      if (event.xexpose.count > 0)
        break;
      putchar('E');
      fflush(stdout);
      regraph(dpy, win, gc, NoFillFace);
      break;
    case ConfigureNotify:
      putchar('C');
      fflush(stdout);
      NoFillFace = reconf(event.xconfigure.width, event.xconfigure.height);
      break;
    case ButtonPress:
      reface(dpy, win, gc, inface(event.xbutton.x, event.xbutton.y));
      break;
    case FocusIn:
      NoFillFace = 0;
      putchar('F');
      fflush(stdout);
      regraph(dpy, win, gc, NoFillFace);
      break;
    case KeyPress:
      end = rekey(dpy, win, gc, &event);
      break;
    default:
      break;
    }
  }
  return end;
}
