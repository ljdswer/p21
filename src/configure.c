#include <configure.h>
#include <display.h>

static XPoint face3[NFACE3][(3 + 1)]; /* 3-top faces top location */
static XPoint face4[NFACE4][(4 + 1)];
static XPoint path4[NPATH4][4];       /* 4-edge path tops location */
static XPoint path5[NPATH5][5];
static XPoint scale;                  /* scale (pixel/cell) for x & y */
static XPoint *vertex;                /* vertex location array address */
static XFace *face;                   /* face array address */
static XPath *edge;                   /* edge path array address */

int relink(XGraph *ph) {
  vertex = ph->vertex;
  edge = ph->edge;
  face = ph->face;
  return (0);
}

/* Check Resize window when configure event */

int resize(unsigned w, unsigned h) {
  static XRectangle bak = {0, 0, 0, 0};
  if ((bak.width == w) && (bak.height == h))
    return (0); /* remove window */
  bak.width = w;
  bak.height = h;
  return (NFACE); /* resize window */
} /* resize */

/* Check window scale when resize */

int rescale(unsigned w, unsigned h) {
  int x, y; /* pixel for cell by x & y */
  x = w / NCELL;
  y = h / NCELL;
  if ((scale.x == x) && (scale.y == y))
    return (0); /* small resize without change scale */
  scale.x = x;
  scale.y = y;
  return (NFACE); /* change scale */
} /* rescale */

/* Rebuild graph configuration */

int rebuild() {            /* depending on graph ! */
  static XPoint vconf[NVERT] = {/* vertex location in x,y cells */
                           {2, 2},
                           {4, 8},
                           {8, 4},
                           {8, 6},
                           {6, 8},
                           {8, 8}};
                           
  for (int i = 6; i < 13; ++i) {
    vconf[i].x = 18 - vconf[i-6].x;
    vconf[i].y = vconf[i-6].y;
  }

  for (int i = 12; i < 24; ++i) {
    vconf[i].x = vconf[i-12].x;
    vconf[i].y = 18 - vconf[i-12].y;
  }

  static int fconf3[NFACE3][(3 + 1)] = {}; /* fconf3 */

  static int fconf4[NFACE4][(4 + 1)] = {
                                  {1, 2, 3, 4, 1},
                                  {8, 9, 10, 7, 8},
                                  {13,16,15,14,13},
                                  {21,22,19,20, 21},
                                  {3, 5, 11, 9, 3},
                                  {11, 10, 22, 23, 11},
                                  {17, 23, 21, 15, 17},
                                  {4, 5, 17, 16, 4}};

  static int pconf4[NPATH4][4] = {                     /* Vertex index */
                                  {0, 1, 2, 0},
                                  {6, 7, 8, 6},
                                  {12, 13, 14, 12},
                                  {18, 19, 20, 18},
                                  {3, 4, 5, 3},
                                  {9, 10, 11, 9},
                                  {15, 16, 17, 15},
                                  {21, 22, 23, 21}};
  static int pconf5[NPATH5][5] = {              /* Vertex index */
                                  {0, 2, 8, 6, 0},
                                  {6, 7, 19, 18, 6},
                                  {12, 14, 20, 18, 12},
                                  {0, 1, 13, 12, 0},
                                  {2, 3, 9, 8, 2},
                                  {1, 4, 16, 13, 1},
                                  {5, 11, 23, 17, 5},
                                  {10, 7, 19, 22, 10},
                                  {15, 21, 20, 14, 15}};

  int i, j;                     /* vertex index */

  for (i = 0; i < NPATH4; ++i) {
    for (j = 0; j < (3 + 1); ++j)
      fconf3[i][j] = pconf4[i][j];
  }

  for (i = 0; i < NPATH5; ++i) {
    for (j = 0; j < (4 + 1); ++j)
      fconf4[i+8][j] = pconf5[i][j];
  }

  for (i = 0; i < NVERT; i++) { /* compute vertex pixel location */
    vertex[i].x = scale.x * vconf[i].x;
    vertex[i].y = scale.y * vconf[i].y;
  }  
                            /* for-vertex */
  for (i = 0; i < NPATH4; i++) /* store vertex pixel location */
    for (j = 0; j < 4; j++) {
      path4[i][j].x = vertex[pconf4[i][j]].x;
      path4[i][j].y = vertex[pconf4[i][j]].y;
    }

  for (i = 0; i < NPATH5; i++) /* store vertex pixel location */
    for (j = 0; j < 5; j++) {
      path5[i][j].x = vertex[pconf5[i][j]].x;
      path5[i][j].y = vertex[pconf5[i][j]].y;
    }

  for (i = 0; i < NFACE3; i++)      /* store vertex pixel location */
    for (j = 0; j < (3 + 1); j++) { /* for 3-top faces */
      face3[i][j].x = vertex[fconf3[i][j]].x;
      face3[i][j].y = vertex[fconf3[i][j]].y;
    } /* for 3-top face */

  for (i = 0; i < NFACE4; i++)      /* store vertex pixel location */
    for (j = 0; j < (4 + 1); j++) { /* for 3-top faces */
      face4[i][j].x = vertex[fconf4[i][j]].x;
      face4[i][j].y = vertex[fconf4[i][j]].y;
    }
  return (0);
} /* rebuild */

/* Trace face & edge path aaray */

int retrace() {                     /* depending on graph ! */
  int f = 0;                        /* total face index */
  int p = 0;                        /* total edge path index */
  int i;                            /* n-edge path index */
  int j;                            /* n-top face index */
  for (j = 0; j < NFACE3; j++) {    /* fix 3-top faces in face array */
    face[f].top = face3[j];         /* fix 3-top face array address */
    face[f].Cn = 3;                 /* fix 3-top face top number=3 */
    face[f].tone = DEFTONE;         /* set face default tone color */
    face[f].regi = XCreateRegion(); /* Empty region for face */
    f++;
  }
  for (j = 0; j < NFACE4; j++) {    /* fix 3-top faces in face array */
    face[f].top = face4[j];         /* fix 3-top face array address */
    face[f].Cn = 4;                 /* fix 3-top face top number=3 */
    face[f].tone = DEFTONE;         /* set face default tone color */
    face[f].regi = XCreateRegion(); /* Empty region for face */
    f++;
  }                            /* face3 */
  face[f].tone = DEFTONE;        /* store extern face default tone color */
  for (i = 0; i < NPATH4; i++) { /* fix 4-edge path in edge path array */
    edge[p].node = path4[i];     /* fix 4-edge path array address */
    edge[p].Pn = 4;              /* fix 4-edge path vertex number=4 */
    p++;
  }

  for (i = 0; i < NPATH5; i++) {
    edge[p].node = path5[i];
    edge[p].Pn = 5;
    p++;
  }
  return (0);
} /* retrace */

/* Reconfigure graph when window resize & rescale */

int reconf(unsigned w, unsigned h) {
  if (resize(w, h) == 0)
    return (0);
  if (rescale(w, h) != 0)
    rebuild();
  return (NFACE);
} /* reconf */

/* Change face region when window resize & rescale */

int rereg() {
  int i;                           /* region & face index */
  static XPoint prescale = {0, 0}; /* previos scale */
  if ((prescale.x == scale.x) && (prescale.y == scale.y))
    return (0);                 /* return, when no change scale */
  for (i = 0; i < NFACE; i++) { /* New region for face */
    XDestroyRegion(face[i].regi);
    face[i].regi = XPolygonRegion(face[i].top, face[i].Cn, 0);
  } /* for */
  prescale.x = scale.x;
  prescale.y = scale.y;
  return (i);
} /* rereg */

/* Check pointed face */

int inface(int x, int y) {
  int f;                      /* face index */
  rereg();                    /* change face region if need */
  for (f = 0; f < NFACE; f++) /* check region with (x,y) inside */
    if (XPointInRegion(face[f].regi, x, y) == True)
      break;
  face[f].tone = (face[f].tone + 1) % NCOLOR; /* new face tone */
  return (f); /* return pointed face for repaint */
} /* inface */
