#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <configure.h>
#include <dispatch.h>
#include <display.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
  XGraph heap;
  Display *dpy = XOpenDisplay(NULL);
  if (!dpy) {
    fprintf(stderr, "Error: Could not open display\n");
    return 1;
  }
  assoc(&heap);
  relink(&heap);
  retrace();
  colorite(dpy);
  GC gc = MakeGC(dpy);
  Window win = WinMain(dpy);
  XStoreName(dpy, win, argv[0]);
  dispatch(dpy, win, gc);
  XDestroyWindow(dpy, win);
  XCloseDisplay(dpy);
  return 0;
}
