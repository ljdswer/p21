#pragma once
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#define NFACE3 8
#define NFACE4 17
#define NFACE NFACE3 + NFACE4 /* all face number */
#define NPATH4 8
#define NPATH5 9
#define NPATH NPATH4 + NPATH5 /* all edge path number */
#define NEDGE 100             /* edge number */
#define NVERT 24              /* vertex number */
#define NCOLOR 4              /* face color number */
#define NCELL 18              /* unit cell number by X & Y */
#define LWIDTH 2              /* contour line width */
#define DOT 8                 /* vertex dot size */
#define DEFTONE 0             /* default face tone */

typedef struct {
  XPoint *top;
  int Cn;
  int tone;
  Region regi;
} XFace;

typedef struct {
  XPoint *node;
  int Pn;
} XPath;

typedef struct {
  XPoint *vertex;
  XFace *face;
  XPath *edge;
} XGraph;

GC MakeGC(Display *);
Window WinMain(Display *);
int colorite(Display *);
int regraph(Display *, Window, GC, int);
int reset(Display *, Window, int);
int reface(Display *, Window, GC, int);
int rekey(Display *, Window, GC, XEvent *);
int assoc(XGraph *);
