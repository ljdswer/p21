NAME=P21
CC=clang
CFLAGS=-std=c99 -g -ggdb -O0
LDFLAGS=-lX11
INCLUDE=-I./include
SRCS=$(wildcard src/*.c)
OBJS=$(patsubst src/%.c, builddir/%.o, $(SRCS))

all: $(NAME)

builddir:
	mkdir builddir

builddir/%.o: src/%.c builddir
	$(CC) $(INCLUDE) $(CFLAGS) -c $< -o $@

$(NAME): $(OBJS)
	$(CC) $(INCLUDE) $(LDFLAGS) $(OBJS) -o $@

.PHONY: clean
clean:
	rm -rf builddir $(NAME) 

.PHONY: run
run: $(NAME)
	./$(NAME)
